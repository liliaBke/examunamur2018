import csv
import pandas as pd
import csv
import matplotlib.pyplot as plt
from collections import Counter

# STEP 1: Import data and create a list

path = '..//Data/'
with open(path + "Coffeebar.csv", "r") as f:  # rename the csv file
    dialect = csv.Sniffer().sniff(f.readline())
    f.seek(0)
    Coffeebar = list(csv.DictReader(f, dialect=dialect))

# STEP 2: What food and drinks are sold by the coffee bar?

# Drinks
list_drinks = [elt['DRINKS'] for elt in Coffeebar]
Drinks1 = set(list_drinks)
print(Drinks1)

# Food
list_food = [elt['FOOD'] for elt in Coffeebar]
Food1 = set(list_food)
print(Food1)

# STEP 3: How many unique customers did the bar have?
path = "..//Data/"
dCoffeebar = pd.read_csv(path + "Coffeebar.csv", sep=";")

seen = {}  # dictionary
dupes = set()
for x in dCoffeebar['CUSTOMER']:  # loop
    print(x)
    if x not in seen:
        seen[x] = 1
    else:
        if seen[x] == 1:
            dupes.add(x)
        seen[x] += 1
unique = set(seen) ^ dupes

# STEP 4: bar plot of the total amount of sold foods (plot1) and drinks (plot2) over the five years

# Food
Food_count = Counter(list_food)
dfood = pd.DataFrame.from_dict(Food_count, orient='index')  # create data frame from the dictionary
dfood.plot(kind='bar')
plt.show()

# Drinks
Drinks_count = Counter(list_drinks)
ddrinks = pd.DataFrame.from_dict(Drinks_count, orient='index')
ddrinks.plot(kind='bar')
plt.show()

# Step 5: Determine the average that a customer buys a certain food or drink at any given time
# Faire un data frame avec les probabilités

list_customer = [elt['CUSTOMER'] for elt in Coffeebar]
Customer = set(list_customer)

dCoffeebar['YEAR'] = dCoffeebar['TIME'].str[0:4]  # alter the data frame to make things easier later
dCoffeebar['DAY'] = dCoffeebar['TIME'].str[5:10]
dCoffeebar['HOUR'] = dCoffeebar['TIME'].str[11:19]

mergingList = []
for index, value in dCoffeebar['YEAR'].iteritems():
    tmpObject = {}
    tmpObject['year'] = value
    tmpObject['day'] = dCoffeebar['DAY'][index]
    tmpObject['hour'] = dCoffeebar['HOUR'][index]
    tmpObject['food'] = list_food[index]
    tmpObject['drink'] = list_drinks[index]
    tmpObject['customer'] = list_customer[index]
    mergingList.append(tmpObject)

print(mergingList)

globalDataFrame = pd.DataFrame(mergingList)  # create data frame
print(globalDataFrame)

# Drinks
Freq_Drink = globalDataFrame.groupby(['hour', 'drink'])  # compute frequencies
Freq_Drink.size() / 1825  # compute probabilities, 1825(5*365)
DfDrink1 = pd.DataFrame(Freq_Drink.size() / 1825)  # data frame with drinks probabilities
print(DfDrink1)
DfDrink = DfDrink1.unstack()  # put data frame correctly
print(DfDrink)
Drink_Probabilities2 = DfDrink.to_csv('Drink_Probabilities2.csv',
                                      sep=';')  # csv file in order to "save" the probabilities

# Food
Freq_Food = globalDataFrame.groupby(['hour', 'food'])
Freq_Food.size() / 1825
DfFood1 = pd.DataFrame(Freq_Food.size() / 1825)
DfFood = DfFood1.unstack()
print(DfFood)
Food_Probabilities2 = DfFood.to_csv('Food_Probabilities2.csv', sep=';')
